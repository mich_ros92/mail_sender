<?php /** @noinspection ReturnTypeCanBeDeclaredInspection */

declare(strict_types=1);

namespace App\Form\Type;

use App\Form\DataClass\ContactDataClass;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class ContactForm
 * @package App\Form\Type
 */
class ContactForm extends AbstractType
{
    public const FORM_NAME = 'contact_form';
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * ContactForm constructor.
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAction($this->router->generate('app_api_customercontactrequest_create'))
            ->setMethod(Request::METHOD_POST)
            ->add('email', EmailType::class)
            ->add('subject', TextType::class, ['required' => false])
            ->add('message', TextareaType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ContactDataClass::class
        ]);
    }

    /**
     * @return null|string
     */
    public function getBlockPrefix()
    {
        return self::FORM_NAME;
    }

}
