<?php

declare(strict_types=1);

namespace App\Form\DataClass;

use App\Model\ApiModelInterface;
use App\Notifier\CustomerContactRequestNotifier\RequestDataInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ContactForm
 * @package App\Form\DataClass
 */
class ContactDataClass implements RequestDataInterface, ApiModelInterface
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string|null
     */
    private $subject;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $message;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)json_encode($this->toArray());
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'email' => $this->email,
            'subject' => $this->subject,
            'message' => $this->message
        ];
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return null|string
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * @param null|string $subject
     */
    public function setSubject(?string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }
}
