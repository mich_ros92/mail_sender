<?php

declare(strict_types=1);

namespace App\Validator;

use App\Exception\ApiValidationException;
use App\Model\InvalidFieldModel;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class Validator
 * @package App\Validator
 */
class Validator
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * Validator constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Wrapper na Symfonowy wbudowany walidator
     *
     * @param $object
     * @param array $validationGroups
     * @throws ApiValidationException
     */
    public function validateByBuiltInValidator($object, array $validationGroups = []): void
    {
        $errors = $this->validator->validate($object, null , $validationGroups);

        $invalidFieldModels = [];
        foreach ($errors as $error) {
            /** @var ConstraintViolation $error */
            $invalidFieldModels[] = new InvalidFieldModel($error->getPropertyPath(), $error->getMessageTemplate());
        }

        if (!empty($invalidFieldModels)) {
            throw new ApiValidationException($invalidFieldModels);
        }
    }
}
