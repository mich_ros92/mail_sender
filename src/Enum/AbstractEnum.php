<?php

declare(strict_types=1);

namespace App\Enum;

/**
 * Class AbstractEnum
 * @package App\Enum
 */
abstract class AbstractEnum
{
    /**
     * @param int|string $typeShortName
     * @return string
     */
    abstract public static function getTypeName($typeShortName): string;

    /**
     * @return array
     */
    abstract public static function getAvailableTypes(): array;

    /**
     * @param $type
     * @return bool
     */
    public static function isExist($type): bool
    {
        return \in_array($type, static::getAvailableTypes(), true);
    }
}