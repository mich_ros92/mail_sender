<?php

declare(strict_types=1);

namespace App\Enum;

use App\Exception\InvalidArgumentException;

/**
 * Class MailSubjectEnum
 * @package App\Enum
 * @see https://www.maxpou.fr/dealing-with-enum-symfony-doctrine/
 */
abstract class MailSubjectEnum extends AbstractEnum
{
    public const SUBJECT_CUSTOMER_CONTACT = 1;

    /** @var string[] */
    protected static $typeName = [
        self::SUBJECT_CUSTOMER_CONTACT => 'email.subject.customer_contact',
    ];

    /**
     * @param int|string $typeShortName
     * @return string
     * @throws InvalidArgumentException
     */
    public static function getTypeName($typeShortName): string
    {
        if (!isset(static::$typeName[$typeShortName])) {
            throw new InvalidArgumentException('MailSubjectEnum', $typeShortName);
        }

        return static::$typeName[$typeShortName];
    }

    /**
     * @return string[]
     */
    public static function getAvailableTypes(): array
    {
        return array(
            self::SUBJECT_CUSTOMER_CONTACT,
        );
    }
}
