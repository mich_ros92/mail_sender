<?php

declare(strict_types=1);

namespace App\Mailer\Sender;

/**
 * Class AbstractMailSender
 * @package App\Mailer\Sender
 */
abstract class AbstractMailSenderData implements MailSenderDataInterface
{
    /**
     * @var string
     */
    protected $senderAddress;

    /**
     * @var string
     */
    protected $senderName;

    /**
     * AbstractMailSender constructor.
     * @param $senderAddress
     * @param $senderName
     */
    public function __construct($senderAddress, $senderName)
    {
        $this->senderAddress = $senderAddress;
        $this->senderName = $senderName;
    }

    /**
     * @return string
     */
    public function getSenderAddress(): string
    {
        return $this->senderAddress;
    }

    /**
     * @return string
     */
    public function getSenderName(): string
    {
        return $this->senderName;
    }
}