<?php

declare(strict_types=1);

namespace App\Mailer\Sender;

/**
 * Interface MailSenderInterface
 * @package App\Mailer\Sender
 */
interface MailSenderDataInterface
{
    /**
     * @return string
     */
    public function getSenderAddress(): string;

    /**
     * @return string
     */
    public function getSenderName(): string;
}