<?php

declare(strict_types=1);

namespace App\Mailer\Sender;

/**
 * Class DefaultMailSender
 * @package App\Mailer\Sender
 */
class DefaultMailSender extends AbstractMailSenderData
{
}