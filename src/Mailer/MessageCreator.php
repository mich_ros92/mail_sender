<?php

declare(strict_types=1);

namespace App\Mailer;

/**
 * Class MessageCreator
 * @package App\Mailer
 */
class MessageCreator
{
    public function createMessageFormEmailModel(EmailNotificationModel $emailNotificationModel): \Swift_Message
    {
        $recipientsAddresses = $emailNotificationModel->getRecipientsAddresses();
        $message = new \Swift_Message($emailNotificationModel->getSubject());
        $message
            ->setTo($recipientsAddresses)
            ->setFormat($emailNotificationModel->getContentFormat())
            ->setBody($emailNotificationModel->getBody());

        if (!empty($attachments = $emailNotificationModel->getAttachments())) {
            // @codeCoverageIgnoreStart
            foreach ($attachments as $attachment) {
                $message->attach(\Swift_Attachment::fromPath($attachment));
            }
            // @codeCoverageIgnoreEnd
        }

        return $message;
    }
}
