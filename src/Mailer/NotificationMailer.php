<?php

declare(strict_types=1);

namespace App\Mailer;

use App\Mailer\Sender\MailSenderDataInterface;
use Psr\Log\LoggerInterface;

/**
 * Class NotificationMailer
 * @package App\Mailer
 */
class NotificationMailer implements NotificationMailerInterface
{
    /**
     * @var MailSenderDataInterface
     */
    private $mailSender;

    /**
     * @var \Swift_Mailer
     */
    private $swiftMailer;

    /**
     * @var MessageCreator
     */
    private $messageCreator;

    /**
     * @var LoggerInterface
     */
    private $mailLogger;

    /**
     * NotificationMailer constructor.
     * @param MailSenderDataInterface $mailSender
     * @param \Swift_Mailer $swiftMailer
     * @param MessageCreator $messageCreator
     * @param LoggerInterface $mailLogger
     */
    public function __construct(
        MailSenderDataInterface $mailSender,
        \Swift_Mailer $swiftMailer,
        MessageCreator $messageCreator,
        LoggerInterface $mailLogger
    ) {
        $this->swiftMailer = $swiftMailer;
        $this->mailLogger = $mailLogger;
        $this->mailSender = $mailSender;
        $this->messageCreator = $messageCreator;
    }

    /**
     * @param EmailNotificationModel $emailNotificationModel
     */
    public function sendNotificationEmail(EmailNotificationModel $emailNotificationModel): void
    {
        $message = $this->messageCreator->createMessageFormEmailModel($emailNotificationModel);
        $message->setFrom($this->mailSender->getSenderAddress(), $this->mailSender->getSenderName());

        $failedRecipients = array();
        $this->swiftMailer->send($message, $failedRecipients);

        if (empty($failedRecipients)) {
            $this->mailLogger->notice('success', [
                'recipients_addresses' => $emailNotificationModel->getRecipientsAddresses()
            ]);
        } else {
            $this->mailLogger->warning('failure, unsuccessful recipients: ', [
                'recipients_addresses' => $emailNotificationModel->getRecipientsAddresses(),
                'failedRecipients' => $failedRecipients
            ]);
        }
    }
}
