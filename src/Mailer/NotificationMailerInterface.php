<?php

declare(strict_types=1);

namespace App\Mailer;

/**
 * Interface NotificationMailerInterface
 * @package App\Mailer
 */
interface NotificationMailerInterface
{
    /**
     * @param EmailNotificationModel $emailNotificationModel
     */
    public function sendNotificationEmail(EmailNotificationModel $emailNotificationModel): void;
}
