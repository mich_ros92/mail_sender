<?php

declare(strict_types=1);

namespace App\Mailer;

/**
 * @codeCoverageIgnore
 * Class EmailNotificationModel
 * @package App\Mailer
 */
class EmailNotificationModel
{
    public const PLAIN_TEXT_FORMAT = 'text/plain';
    public const HTML_FORMAT = 'text/html';

    /**
     * @var string[]
     */
    private $recipientsAddresses;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $contentFormat;

    /**
     * @var string
     */
    private $body;

    /**
     * @var string[]
     */
    private $attachments;

    /**
     * EmailNotificationModel constructor.
     * @param string[] $recipientsAddresses [$addresses => $name]
     * @param string $subject
     * @param string $contentFormat
     * @param string $body
     * @param string[] $attachments
     */
    public function __construct(array $recipientsAddresses, $subject, $contentFormat, $body, array $attachments = array())
    {
        $this->recipientsAddresses = $recipientsAddresses;
        $this->subject = $subject;
        $this->contentFormat = $contentFormat;
        $this->body = $body;
        $this->attachments = $attachments;
    }

    /**
     * @return string[]
     */
    public function getRecipientsAddresses(): array
    {
        return $this->recipientsAddresses;
    }

    /**
     * @param string[] $recipientsAddresses
     */
    public function setRecipientsAddresses($recipientsAddresses): void
    {
        $this->recipientsAddresses = $recipientsAddresses;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body): void
    {
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getContentFormat(): string
    {
        return $this->contentFormat;
    }

    /**
     * @param string $contentFormat
     */
    public function setContentFormat($contentFormat): void
    {
        $this->contentFormat = $contentFormat;
    }

    /**
     * @return string[]
     */
    public function getAttachments(): array
    {
        return $this->attachments;
    }

    /**
     * @param string[] $attachments
     */
    public function setAttachments($attachments): void
    {
        $this->attachments = $attachments;
    }
}