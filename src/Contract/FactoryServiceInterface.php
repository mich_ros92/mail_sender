<?php

declare(strict_types=1);

namespace App\Contract;

/**
 * Class FactoryServiceInterface
 * @package App\Contract
 */
interface FactoryServiceInterface
{
    /**
     * @return string
     */
    public function getServiceName(): string;
}