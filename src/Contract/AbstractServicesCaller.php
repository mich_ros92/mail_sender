<?php

declare(strict_types=1);

namespace App\Contract;

use App\Exception\ServicesRegistrationException;

/**
 * Class AbstractServicesCaller
 * @package App\Contract
 */
abstract class AbstractServicesCaller
{
    /**
     * @var FactoryServiceInterface[]
     */
    protected $servicesArray;

    /**
     * AbstractServicesCaller constructor.
     */
    public function __construct()
    {
        $this->servicesArray = array();
    }

    /**
     * @param FactoryServiceInterface $factoryService
     * @param $className
     * @throws ServicesRegistrationException
     */
    protected function assignService(FactoryServiceInterface $factoryService, $className): void
    {
        $factoryServiceName = $factoryService->getServiceName();
        if (!array_key_exists($factoryServiceName, $this->servicesArray)) {
            $this->servicesArray[$factoryServiceName] = $factoryService;
        } else {
            throw new ServicesRegistrationException('Something wrong with declared service\'s. Service ' . $factoryServiceName . ' is already registered in ' . $className);
        }
    }
}