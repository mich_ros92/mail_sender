<?php

declare(strict_types=1);

namespace App\Controller\ApiController;

use App\Exception\ApiValidationException;
use App\Form\DataClass\ContactDataClass;
use App\Notifier\CustomerContactRequestNotifier\CustomerContactRequestNotifierCaller;
use App\Validator\Validator;
use FOS\RestBundle\Controller\Annotations as Rest;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class GeneralApiController
 * @package App\Controller\ApiController
 */
class CustomerContactRequestController
{
    /**
     * @var LoggerInterface
     */
    private $apiLogger;

    /**
     * CustomerContactRequestController constructor.
     * @param LoggerInterface $apiLogger
     */
    public function __construct(LoggerInterface $apiLogger)
    {
        $this->apiLogger = $apiLogger;
    }

    /**
     * @Rest\Post("/create")
     * @ParamConverter("contactData", converter="fos_rest.request_body")
     * @param ContactDataClass $contactData
     * @param CustomerContactRequestNotifierCaller $notifierCaller
     * @param Validator $validator
     * @return Response
     */
    public function create(
        ContactDataClass $contactData,
        CustomerContactRequestNotifierCaller $notifierCaller,
        Validator $validator
    ): Response {

        try {
            $validator->validateByBuiltInValidator($contactData);
        } catch (ApiValidationException $exception) {
            $this->apiLogger->error('Invalid data', [
                'context' => 'validation',
                'object' => $contactData,
            ]);

            return new JsonResponse($exception->getInvalidFieldModels(), $exception->getCode());
        }

        $notifierCaller->callNotifier($contactData);

        return new JsonResponse([], Response::HTTP_OK);
    }
}
