<?php

declare(strict_types=1);

namespace App\Controller\HtmlController;

use App\Form\Type\ContactForm;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

/**
 * Class MainController
 * @package App\Controller\HtmlController
 */
class MainController
{
    /**
     * @var Environment
     */
    private $twig;
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * MainController constructor.
     * @param Environment $twig
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(
        Environment $twig,
        FormFactoryInterface $formFactory
    ) {
        $this->twig = $twig;
        $this->formFactory = $formFactory;
    }

    /**
     * @return Response
     */
    public function index(): Response
    {
        $contactForm = $this->formFactory->create(ContactForm::class);
        $content = $this->twig->render(
            'main/index.html.twig',
            ['contact_form' => $contactForm->createView()]
        );

        return new Response($content);
    }
}
