<?php

declare(strict_types=1);

namespace App\Model;

/**
 * Interface ApiModelInterface
 * @package App\Model
 */
interface ApiModelInterface
{
    /**
     * @return string
     */
    public function __toString(): string;

    /**
     * @return array
     */
    public function toArray(): array;
}
