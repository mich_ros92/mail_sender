<?php

declare(strict_types=1);

namespace App\Model;

/**
 * Class InvalidFieldModel
 * @package App\Model
 */
class InvalidFieldModel implements ApiModelInterface
{
    /**
     * @var string
     */
    private $invalidFieldName;

    /**
     * @var string
     */
    private $invalidFieldDescription;

    /**
     * InvalidFieldModel constructor.
     * @param $invalidFieldName
     * @param $invalidFieldDescription
     */
    public function __construct($invalidFieldName, $invalidFieldDescription)
    {
        $this->invalidFieldName = $invalidFieldName;
        $this->invalidFieldDescription = $invalidFieldDescription;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)json_encode($this->toArray());
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'invalidFieldName' => $this->invalidFieldName,
            'invalidFieldDescription' => $this->invalidFieldDescription
        ];
    }

    /**
     * @return string
     */
    public function getInvalidFieldName(): string
    {
        return $this->invalidFieldName;
    }

    /**
     * @param string $invalidFieldName
     */
    public function setInvalidFieldName($invalidFieldName): void
    {
        $this->invalidFieldName = $invalidFieldName;
    }

    /**
     * @return string
     */
    public function getInvalidFieldDescription(): string
    {
        return $this->invalidFieldDescription;
    }

    /**
     * @param string $invalidFieldDescription
     */
    public function setInvalidFieldDescription($invalidFieldDescription): void
    {
        $this->invalidFieldDescription = $invalidFieldDescription;
    }
}