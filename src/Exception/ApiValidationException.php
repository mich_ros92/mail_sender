<?php

declare(strict_types=1);

namespace App\Exception;

use App\Model\InvalidFieldModel;
use Symfony\Component\HttpFoundation\Response;

/**
 * Klasa wyrzucająca błędy walidacji
 *
 * Class ApiValidationException
 * @package App\Exception
 * @codeCoverageIgnore
 */
class ApiValidationException extends \Exception
{
    /**
     * @var InvalidFieldModel[]
     */
    private $invalidFieldModels;

    /**
     * ApiValidationException constructor.
     * @param InvalidFieldModel[] $invalidFieldModels
     */
    public function __construct(array $invalidFieldModels = array())
    {
        $this->invalidFieldModels = $invalidFieldModels;

        parent::__construct('validation.default.error', Response::HTTP_UNPROCESSABLE_ENTITY, null);
    }

    /**
     * @return InvalidFieldModel[]
     */
    public function getInvalidFieldModels(): array
    {
        return $this->invalidFieldModels;
    }
}