<?php

declare(strict_types=1);

namespace App\Exception;

use Throwable;

/**
 * Class ServicesRegistrationException
 * @package App\Exception
 */
class ServicesRegistrationException extends \Exception
{
    public const NO_SERVICES_REGISTERED_MESSAGE = 'There are not any services registered in services.yml';

    /**
     * ServiceNotRegisteredException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = self::NO_SERVICES_REGISTERED_MESSAGE, $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
