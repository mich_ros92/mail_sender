<?php

declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * Class InvalidArgumentException
 * @package App\Exception
 */
class InvalidArgumentException extends \Exception
{
    public function __construct(string $expectedArg, $givenArg, Throwable $previous = null)
    {
        parent::__construct($this->buildMessage($expectedArg, $givenArg), Response::HTTP_BAD_REQUEST, $previous);
    }

    /**
     * @param string $expectedArg
     * @param mixed $givenArg
     * @return string
     */
    private function buildMessage(string $expectedArg, $givenArg): string
    {
        $typeOfGivenArg = \gettype($givenArg);
        return 'Expected ' . $expectedArg . ', got ' . ($typeOfGivenArg !== 'object' ? $typeOfGivenArg : \get_class($givenArg));
    }
}