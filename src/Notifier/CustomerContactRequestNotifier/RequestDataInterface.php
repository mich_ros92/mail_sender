<?php

declare(strict_types=1);

namespace App\Notifier\CustomerContactRequestNotifier;

/**
 * interface RequestDataInterface
 * @package App\Notifier\CustomerContactRequestNotifier
 */
interface RequestDataInterface
{
    /**
     * @return string
     */
    public function getEmail(): string;

    /**
     * @return null|string
     */
    public function getSubject(): ?string;

    /**
     * @return string
     */
    public function getMessage(): string;
}
