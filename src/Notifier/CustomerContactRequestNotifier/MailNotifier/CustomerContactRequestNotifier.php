<?php

declare(strict_types=1);

namespace App\Notifier\CustomerContactRequestNotifier\MailNotifier;

use App\Enum\MailSubjectEnum;
use App\Mailer\EmailNotificationModel;
use App\Mailer\NotificationMailerInterface;
use App\Mailer\Sender\MailSenderDataInterface;
use App\Notifier\CustomerContactRequestNotifier\CustomerContactRequestNotifierServiceInterface;
use App\Notifier\CustomerContactRequestNotifier\RequestDataInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Twig_Environment;

/**
 * Class CustomerContactRequestNotifier
 * @package App\Notifier\CustomerContactRequestNotifier\MailNotifier
 */
class CustomerContactRequestNotifier implements CustomerContactRequestNotifierServiceInterface
{
    /**
     * @var Twig_Environment
     */
    private $twig;
    /**
     * @var MailSenderDataInterface
     */
    private $mailSenderData;
    /**
     * @var NotificationMailerInterface
     */
    private $mailer;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CustomerContactRequestNotifier constructor.
     * @param Twig_Environment $twig
     * @param TranslatorInterface $translator
     * @param MailSenderDataInterface $mailSenderData
     * @param NotificationMailerInterface $mailer
     */
    public function __construct(
        Twig_Environment $twig,
        TranslatorInterface $translator,
        MailSenderDataInterface $mailSenderData,
        NotificationMailerInterface $mailer
    ) {
        $this->twig = $twig;
        $this->mailSenderData = $mailSenderData;
        $this->mailer = $mailer;
        $this->translator = $translator;
    }

    /**
     * @param RequestDataInterface $requestData
     * @throws \App\Exception\InvalidArgumentException
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function notify(RequestDataInterface $requestData): void
    {
        $body = $this->twig->render('email/contact_mail.html.twig', [
            'request_data' => $requestData
        ]);

        $subject = $this->translator->trans(
            MailSubjectEnum::getTypeName(MailSubjectEnum::SUBJECT_CUSTOMER_CONTACT), [], 'messages', 'pl'
        );

        $mailData = new EmailNotificationModel(
            [$this->mailSenderData->getSenderAddress() => $this->mailSenderData->getSenderName()],
            $subject,
            EmailNotificationModel::HTML_FORMAT,
            $body
        );

        $this->mailer->sendNotificationEmail($mailData);
    }

    /**
     * @return string
     */
    public function getServiceName(): string
    {
        return self::class;
    }
}
