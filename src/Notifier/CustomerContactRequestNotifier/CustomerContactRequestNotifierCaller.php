<?php

declare(strict_types=1);

namespace App\Notifier\CustomerContactRequestNotifier;

use App\Contract\AbstractServicesCaller;
use App\Exception\ServicesRegistrationException;

/**
 * Class CustomerContactRequestNotifierCaller
 * @package App\Notifier\CustomerContactRequestNotifier
 */
class CustomerContactRequestNotifierCaller extends AbstractServicesCaller
{
    /**
     * @var CustomerContactRequestNotifierServiceInterface[]
     */
    protected $servicesArray;

    /**
     * @param RequestDataInterface $requestData
     */
    public function callNotifier(RequestDataInterface $requestData): void
    {
        foreach ($this->servicesArray as $requestNotifierService) {
            $requestNotifierService->notify($requestData);
        }
    }

    /**
     * @param CustomerContactRequestNotifierServiceInterface $service
     * @throws ServicesRegistrationException
     */
    public function assignNotifier(CustomerContactRequestNotifierServiceInterface $service): void
    {
        parent::assignService($service, self::class);
    }
}
