<?php

declare(strict_types=1);

namespace App\Notifier\CustomerContactRequestNotifier;

use App\Contract\FactoryServiceInterface;

/**
 * Interface CustomerContactRequestNotifierServiceInterface
 * @package App\Notifier\CustomerContactRequestNotifier
 */
interface CustomerContactRequestNotifierServiceInterface extends FactoryServiceInterface
{
    /**
     * @param RequestDataInterface $requestData
     */
    public function notify(RequestDataInterface $requestData): void;
}
