'use strict';

(function(window) {
    window.RequestSender = {
        request: function(data) {
            return fetch(data.url, {
                method: data.method,
                headers: {
                    "Content-Type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(data.body),
            })
        }
    }
})(window);
