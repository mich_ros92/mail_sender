'use strict';

(function(window) {
    window.contactFormApp = {
        initialize: function() {
            console.log('weszlo');
            this.contactModalContainer = window.document.getElementById("contact-form-modal-container");
            this.contactModalShowBtn = window.document.getElementsByClassName("js-contact-form-open-btn")[0];
            this.contactModalCloseBtn = window.document.getElementsByClassName("js-contact-form-close-btn")[0];
            this.contactModalSendBtn = window.document.getElementsByClassName("js-contact-form-send-btn")[0];
            this.contactForm = window.document.getElementsByClassName("js-contact-form")[0];

            this.contactModalShowBtn.onclick = this.showModal.bind(this);
            this.contactModalCloseBtn.onclick = this.closeModal.bind(this);
            window.addEventListener("click", function(event) {
                this.closeModalEvent(event);
            }.bind(this));

            this.contactModalSendBtn.onclick = this.sendMessage.bind(this);
        },

        showModal: function() {
            console.log('showModal');
            this.contactModalContainer.style.display = "block";
        },

        closeModal: function() {
            console.log('closeModal');
            this.contactModalContainer.style.display = "none";
        },

        closeModalEvent: function(event) {
            console.log('closeModalEvent');
            if (event.target === this.contactModalContainer) {
                this.closeModal();
            }
        },

        sendMessage: function() {
            console.log('sendMessage');

            let inputs = this.contactForm.getElementsByClassName('form-control');
            this.validateContactRequest(inputs);
            if (!this.validateContactRequest(inputs))
            {
                console.log('invalid message');
                return;
            }
            let data = {};
            data['body'] = this.mapFormBody(inputs);
            data['url'] = this.contactForm.action;
            data['method'] = this.contactForm.method;

            window.RequestSender.request(data)
                .then(function(response) {
                    console.log(response);
                    console.log(this);
                    if (response.ok) {
                        window.contactFormApp.closeModal();
                    }
                });

        },

        mapFormBody: function(inputs) {
            let data = {};
            let re = /\[(.*?)\]/;
            for (var i = 0; i < inputs.length; i++) {
                data[re.exec(inputs[i].name)[1]] = inputs[i].value;
            }
            return data;
        },

        validateContactRequest: function(inputs) {
            let isValid = true;
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].required && !window.Validator.validateNotBlank(inputs[i].value)) {
                    isValid = false;
                    console.log('element blank: ' + inputs[i].name)
                }

                if (inputs[i].type === 'email' && !window.Validator.validateEmail(inputs[i].value)) {
                    isValid = false;
                    console.log('wrong email: ' + inputs[i].value)
                }
            }
            return isValid;
        }
    }
})(window);