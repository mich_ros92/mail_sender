<?php

declare(strict_types=1);

namespace App\Tests\Mailer;

use App\Mailer\EmailNotificationModel;
use App\Mailer\MessageCreator;
use App\Mailer\NotificationMailer;
use App\Mailer\Sender\MailSenderDataInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

/**
 * Class NotificationMailerTest
 * @package App\Tests\Mailer
 * @group unit
 */
class NotificationMailerTest extends TestCase
{
    /**
     * @var MailSenderDataInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $mailSenderDataMock;

    /**
     * @var \Swift_Mailer|\PHPUnit_Framework_MockObject_MockObject
     */
    private $swiftMailerMock;

    /**
     * @var MessageCreator|\PHPUnit_Framework_MockObject_MockObject
     */
    private $messageCreatorMock;

    /**
     * @var LoggerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $loggerMock;

    /**
     * @var NotificationMailer
     */
    private $notificationMailer;

    public function setUp()
    {
        $this->mailSenderDataMock = $this->createConfiguredMock(MailSenderDataInterface::class, [
            'getSenderAddress' => 'example@mail.com',
            'getSenderName' => 'Example'
        ]);
        $this->swiftMailerMock = $this->createMock(\Swift_Mailer::class);
        $this->messageCreatorMock = $this->createMock(MessageCreator::class);
        $this->loggerMock = $this->createMock(LoggerInterface::class);
        $this->notificationMailer = new NotificationMailer(
            $this->mailSenderDataMock,
            $this->swiftMailerMock,
            $this->messageCreatorMock,
            $this->loggerMock
        );
    }

    /**
     * @test
     */
    public function sendNotificationEmail_LoggedFailedRecipients_WhenSendMessageFail(): void
    {
        $recipients = array();
        $emailNotificationModel = new EmailNotificationModel($recipients, 'testSubject', EmailNotificationModel::PLAIN_TEXT_FORMAT, 'testBody', array());

        $swiftMessageMock = $this->createMock(\Swift_Message::class);
        $swiftMessageMock
            ->expects($this->once())
            ->method('setFrom')
            ->with($this->mailSenderDataMock->getSenderAddress(), $this->mailSenderDataMock->getSenderName());

        $this->messageCreatorMock
            ->expects($this->once())
            ->method('createMessageFormEmailModel')
            ->with($emailNotificationModel)
            ->willReturn($swiftMessageMock);

        $this->swiftMailerMock
            ->expects($this->once())
            ->method('send')
            ->with($swiftMessageMock, [])
            ->willReturnCallback(function ($message, &$failedRecipients) {
                $failedRecipients[] = 'example@mail.com';
            });

        $this->loggerMock
            ->expects($this->once())
            ->method('warning');

        $this->notificationMailer->sendNotificationEmail($emailNotificationModel);
    }

    /**
     * @test
     */
    public function sendNotificationEmail_LoggedSuccess_WhenMessageSend(): void
    {
        $recipients = array();
        $emailNotificationModel = new EmailNotificationModel($recipients, 'testSubject', EmailNotificationModel::PLAIN_TEXT_FORMAT, 'testBody', array());

        $swiftMessageMock = $this->createMock(\Swift_Message::class);
        $swiftMessageMock
            ->expects($this->once())
            ->method('setFrom')
            ->with($this->mailSenderDataMock->getSenderAddress(), $this->mailSenderDataMock->getSenderName());

        $this->messageCreatorMock
            ->expects($this->once())
            ->method('createMessageFormEmailModel')
            ->with($emailNotificationModel)
            ->willReturn($swiftMessageMock);

        $this->swiftMailerMock
            ->expects($this->once())
            ->method('send')
            ->with($swiftMessageMock, []);

        $this->loggerMock
            ->expects($this->once())
            ->method('notice');

        $this->notificationMailer->sendNotificationEmail($emailNotificationModel);
    }
}
