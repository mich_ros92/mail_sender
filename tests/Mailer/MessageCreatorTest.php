<?php

declare(strict_types=1);

namespace App\Tests\Mailer;

use App\Mailer\EmailNotificationModel;
use App\Mailer\MessageCreator;
use PHPUnit\Framework\TestCase;

/**
 * Class MessageCreatorTest
 * @package App\Tests\Mailer
 * @group unit
 */
class MessageCreatorTest extends TestCase
{
    /**
     * @var MessageCreator
     */
    private $messageCreator;

    protected function setUp()
    {
        $this->messageCreator = new MessageCreator();
    }

    /**
     * @test
     */
    public function createMessageFormEmailModel_WillReturnSwiftMessage_WhenPassedCorrectModel(): void
    {
        $expectRecipients = ['example@mail.com' => 'Example'];
        $expectSubject = 'testSubject';
        $expectContentFormat = EmailNotificationModel::PLAIN_TEXT_FORMAT;
        $expectBody = 'testBody';
        $expectAttachments = [];
        $emailNotificationModel = new EmailNotificationModel(
            $expectRecipients,
            $expectSubject,
            $expectContentFormat,
            $expectBody,
            $expectAttachments
        );

        $swiftMessage = $this->messageCreator->createMessageFormEmailModel($emailNotificationModel);

        $this->assertEquals($expectRecipients, $swiftMessage->getTo());
        $this->assertEquals($expectSubject, $swiftMessage->getSubject());
        $this->assertEquals($expectContentFormat, $swiftMessage->getFormat());
        $this->assertEquals($expectBody, $swiftMessage->getBody());
        $this->assertEquals($expectAttachments, $swiftMessage->getChildren());
    }
}
