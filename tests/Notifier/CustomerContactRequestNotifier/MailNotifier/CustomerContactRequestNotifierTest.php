<?php

declare(strict_types=1);

namespace App\Tests\Notifier\CustomerContactRequestNotifier\MailNotifier;

use App\Enum\MailSubjectEnum;
use App\Exception\InvalidArgumentException;
use App\Mailer\EmailNotificationModel;
use App\Mailer\NotificationMailerInterface;
use App\Mailer\Sender\MailSenderDataInterface;
use App\Notifier\CustomerContactRequestNotifier\MailNotifier\CustomerContactRequestNotifier;
use App\Notifier\CustomerContactRequestNotifier\RequestDataInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Translation\TranslatorInterface;
use Twig_Environment;

/**
 * Class CustomerContactRequestNotifierTest
 * @package App\Tests\Notifier\CustomerContactRequestNotifier\MailNotifier
 * @group unit
 */
class CustomerContactRequestNotifierTest extends TestCase
{
    /**
     * @var Twig_Environment|\PHPUnit_Framework_MockObject_MockObject
     */
    private $twigMock;

    /**
     * @var TranslatorInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $translatorMock;

    /**
     * @var MailSenderDataInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $mailSenderDataMock;

    /**
     * @var NotificationMailerInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $notificationMailerMock;

    /**
     * @var CustomerContactRequestNotifier
     */
    private $customerContactRequestNotifier;

    protected function setUp()
    {
        $this->twigMock = $this->createMock(Twig_Environment::class);
        $this->translatorMock = $this->createMock(TranslatorInterface::class);
        $this->mailSenderDataMock = $this->createConfiguredMock(MailSenderDataInterface::class, [
            'getSenderAddress' => 'example@mail.com',
            'getSenderName' => 'Example'
        ]);
        $this->notificationMailerMock = $this->createMock(NotificationMailerInterface::class);

        $this->customerContactRequestNotifier = new CustomerContactRequestNotifier(
            $this->twigMock,
            $this->translatorMock,
            $this->mailSenderDataMock,
            $this->notificationMailerMock
        );
    }

    /**
     * @test
     * @throws InvalidArgumentException
     */
    public function notify_WillCallMailSender_WithCorrectMessageData(): void
    {
        $expectRecipients = [$this->mailSenderDataMock->getSenderAddress() => $this->mailSenderDataMock->getSenderName()];
        $expectSubject = 'testSubject';
        $expectContentFormat = EmailNotificationModel::HTML_FORMAT;
        $expectBody = 'testBody';
        $expectAttachments = [];
        $emailNotificationModel = new EmailNotificationModel(
            $expectRecipients,
            $expectSubject,
            $expectContentFormat,
            $expectBody,
            $expectAttachments
        );

        /** @var RequestDataInterface|\PHPUnit_Framework_MockObject_MockObject $requestData */
        $requestData = $this->createMock(RequestDataInterface::class);

        $this->twigMock
            ->expects($this->once())
            ->method('render')
            ->with('email/contact_mail.html.twig', ['request_data' => $requestData])
            ->willReturn($expectBody);

        $this->translatorMock
            ->expects($this->once())
            ->method('trans')
            ->with(MailSubjectEnum::getTypeName(MailSubjectEnum::SUBJECT_CUSTOMER_CONTACT), [], 'messages', 'pl')
            ->willReturn($expectSubject);

        $this->notificationMailerMock
            ->expects($this->once())
            ->method('sendNotificationEmail')
            ->with($emailNotificationModel);

        $this->customerContactRequestNotifier->notify($requestData);
    }

    /**
     * @test
     */
    public function getServiceName_WillReturnCorrectClassName(): void
    {
        $serviceName = $this->customerContactRequestNotifier->getServiceName();

        $this->assertEquals(CustomerContactRequestNotifier::class, $serviceName);
    }
}

