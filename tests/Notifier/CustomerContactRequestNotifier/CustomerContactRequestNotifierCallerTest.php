<?php

declare(strict_types=1);

namespace App\Tests\Notifier\CustomerContactRequestNotifier;

use App\Exception\ServicesRegistrationException;
use App\Notifier\CustomerContactRequestNotifier\CustomerContactRequestNotifierCaller;
use App\Notifier\CustomerContactRequestNotifier\CustomerContactRequestNotifierServiceInterface;
use App\Notifier\CustomerContactRequestNotifier\RequestDataInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class CustomerContactRequestNotifierCallerTest
 * @package App\Tests\Notifier\CustomerContactRequestNotifier
 * @group unit
 */
class CustomerContactRequestNotifierCallerTest extends TestCase
{
    /**
     * @var CustomerContactRequestNotifierServiceInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $assignedNotifierServiceMock;

    /**
     * @var CustomerContactRequestNotifierCaller
     */
    private $notifierCaller;

    /**
     * @throws ServicesRegistrationException
     */
    public function setUp()
    {
        $this->notifierCaller = new CustomerContactRequestNotifierCaller();

        $this->assignedNotifierServiceMock = $this->createMock(CustomerContactRequestNotifierServiceInterface::class);
        $this->assignedNotifierServiceMock
            ->expects($this->any())
            ->method('getServiceName')
            ->willReturn(CustomerContactRequestNotifierServiceInterface::class);
        $this->notifierCaller->assignNotifier($this->assignedNotifierServiceMock);
    }

    /**
     * @test
     */
    public function callNotifier_WillCallAssignedService(): void
    {
        /** @var RequestDataInterface|\PHPUnit_Framework_MockObject_MockObject $requestData */
        $requestData = $this->createMock(RequestDataInterface::class);

        $this->assignedNotifierServiceMock
            ->expects($this->once())
            ->method('notify')
            ->with($requestData);

        $this->notifierCaller->callNotifier($requestData);
    }

    /**
     * @test
     * @throws ServicesRegistrationException
     */
    public function assignNotifier_WillThrowException_WhenAssignTwiceTheSameService(): void
    {
        $this->expectException(ServicesRegistrationException::class);

        $this->notifierCaller->assignNotifier($this->assignedNotifierServiceMock);
    }
}
