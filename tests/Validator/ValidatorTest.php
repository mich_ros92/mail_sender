<?php

declare(strict_types=1);

namespace App\Test\Validator;

use App\Exception\ApiValidationException;
use App\Validator\Validator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ValidatorTest
 * @package Unit\Validator
 * @group unit
 */
class ValidatorTest extends TestCase
{
    /**
     * @var Validator
     */
    private $validator;

    /**
     * @var ValidatorInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $validatorMock;

    public function setUp()
    {
        $this->validatorMock = $this->createMock(ValidatorInterface::class);

        $this->validator = new Validator($this->validatorMock);
    }

    /**
     * @test
     * @throws ApiValidationException
     */
    public function validateByBuiltInValidatorWillThrowExceptionWhenHasErrors(): void
    {
        $object = null;
        $validationsGroup = array();

        $this->expectException(ApiValidationException::class);

        $error = $this->createMock(ConstraintViolation::class);

        $this->validatorMock
            ->expects($this->once())
            ->method('validate')
            ->willReturn(array($error));

        $this->validator->validateByBuiltInValidator($object, $validationsGroup);
    }

    /**
     * @test
     * @throws ApiValidationException
     */
    public function validateByBuiltInValidatorWillValidateObject(): void
    {
        $object = null;
        $validationsGroup = array();

        $this->validatorMock
            ->expects($this->once())
            ->method('validate')
            ->willReturn(array());

        $this->validator->validateByBuiltInValidator($object, $validationsGroup);
    }
}