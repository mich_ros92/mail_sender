mail_sender
=======
## Instalacja ##

`composer install` (wymagany composer zainstalowany globalnie)
    
`composer.phar install` (jeśli composer zainstalowany lokalnie)

W pliku  `.env` zdefiniować parametry skrzynki pocztowej
        
Start serwera:
    
    `php ./bin/console server:run`